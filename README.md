# Auto Wifi in Chang-Gung University

This app allows user login to Chang-Gung University's wifi hotspot [cgu-wlan] automatic. 



## Install

Just build it and put it into your android device.

First use please turn on the option [自動登入], then the app would login when finding the [cgu-wlan].


## Google Play store

You can find this app here: https://play.google.com/store/apps/details?id=tw.instartit.app.autowificgu


## Content me

Mail to johnny5581@gmail.com or nagiyamazaki@hotmail.com .

Or find me in Management-Building @ CGU(Taiwan). :)


## About

CGU: http://www.cgu.edu.tw/
